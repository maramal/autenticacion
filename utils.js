const jwt = require('jsonwebtoken');

module.exports = {
    validateToken: (req, res, next) => {
        const authorizationHeader = req.headers.authorization;
        let result;
        if (authorizationHeader) {
            const token = req.headers.authorization.split(' ')[1]; // Bearer <token>
            const options = {
                expiresIn: process.env.JWT_EXPIRES,
                issuer: process.env.JWT_ISSUER
            };

            console.table(token);

            try {
                // Se verifica que el token no está expirado y que fue entregado por el ISSUER
                result = jwt.verify(token, process.env.JWT_SECRET, options);

                // Renovamos el token a la solicitud
                req.decoded = result;

                next();
            } catch(err) {
                throw new Error(err);
            }
        } else {
            status = 401;
            result = {
                error: `Error de autenticación. El token es requerido`,
                status: status
            }
            res.status(status)
             .send(result);
        }
    }
};