const mongoose = require('mongoose');
const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const connURI = process.env.MONGO_LOCAL_CONN_URL;

module.exports = {
    add: (req, res) => {
        mongoose.connect(connURI, { useNewUrlParser: true }, err => {
            let result = {};
            let status = 200;

            if (!err) {
                const { username, password } = req.body;
                const user = new User({ username, password });

                user.save((err, user) => {
                    if (!err) {
                        result.status = status;
                        result.result = user;
                    } else {
                        status = 500;
                        result.status = status;
                        result.error = err;
                        result.text = `El usuario no pudo ser guardado`;
                    }
                    res.status(status)
                       .send(result);
                });
            } else {
                status = 500;
                result.status = status;
                result.error = error;
                result.text = `Hubo un problema al intentar conectarse a la base de datos`;
                res.status(status)
                   .send(result);
            }
        });
    },
    login: (req, res) => {
        const { username, password } = req.body;

        mongoose.connect(connURI, { useNewUrlParser: true }, err => {
            let result = {};
            let status = 200;

            if (!err) {
                User.findOne({ username }, (err, user) => {
                    if (!err && user) {
                        bcrypt.compare(password, user.password)
                         .then(match => {
                            if (match) {
                                // Creación de Webtoken
                                const payload = { user: username };
                                const options = {
                                    expiresIn: process.env.JWT_EXPIRES,
                                    issuer: process.env.JWT_ISSUER
                                };
                                const secret = process.env.JWT_SECRET;
                                const token = jwt.sign(payload, secret, options);

                                console.log('TOKEN', token);
                                result.token = token;
                                result.status = status;
                                result.result = user;
                            } else {
                                status = 401;
                                result.status = status;
                                result.error = 'Error de Autenticación';
                                result.text = `No se encontró el usuario`;
                            }
                            res.status(status)
                             .send(result);
                        }).catch(err => {
                            status = 500;
                            result.status = status;
                            result.error = err;
                            result.text = `Su contraseña no es correcta`;
                            res.status(status)
                             .send(result);
                         });
                    } else {
                        status = 404;
                        result.status = status;
                        result.error = err;
                        result.text = `Hubo un problema al buscar el usuario`;
                        res.status(status)
                         .send(result);
                    }
                });
            } else {
                status = 500;
                result.status = status;
                result.error = err;
                result.text = `Hubo un problema al intentar conectarse a la base de datos`;
                res.status(status)
                 .send(result);
            }
        })
    },
    getAll: (req, res) => {
        mongoose.connect(connURI, { useNewUrlParser: true }, err => {
            let result = {};
            let status = 200;

            if (!err) {
                const payload = req.decoded;
                console.log('PAYLOAD', payload);
                if (payload && payload.user === 'admin') {
                    User.find({}, (err, users) => {
                        if (!err) {
                            result.status = status;
                            result.error = err;
                            result.result = users;
                        } else {
                            status = 500;
                            result.status = status;
                            result.error = err;
                            result.text = `Hubo un problema al listar los usuarios`;
                        }
                        res.status(status)
                         .send(result);
                    });
                } else {
                    status = 401;
                    result.status = status;
                    result.error = `Error de autenticación`;
                    result.text = `El usuario no es administrador`;
                    res.status(status)
                     .send(result);
                }
            } else {
                status = 500;
                result.status = status;
                result.error = err;
                result.text = `Hubo un problema al intentar conectarse a la base de datos`;
                res.status(status)
                 .send(result);
            }
        });
    }
};